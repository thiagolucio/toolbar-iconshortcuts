# toolbar-iconshortcuts package

A more complete option of shortcut icons to the suda toolbar main for ATOM Editor.

Package Dependencies - It must have installed a toolbar SUDA extension of ATOM before(pre requisite)...[tool-bar](https://atom.io/packages/tool-bar) package.

![Tool bar Icon Shortcuts preview](http://www.thiagolucio.com.br/images/toolbar-iconshortcuts.jpg)

# Buttons

* New File
* Open...
* Open Project...
* Save
* Find in Buffer
* Replace in Buffer
* Toggle Command Palette

* Split Pane Right
* Toggle Tree-View
* Preview inside ATOM
* Open in Browser... Need install before the "Open in Browser" Package. Search on instal package or goto - https://github.com/magbicaleman/open-in-browser

* Settings View
* Reload Window
* Toggle Developer Tools
